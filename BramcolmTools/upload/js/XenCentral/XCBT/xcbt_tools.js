$(function(){
    $(document).on('click','.actionBar-action--briefing-add', function(event) {
        event.preventDefault();
        button = $(this);
        var url = button.attr('data-href');

        var serializedData;

        serializedData = [];

        serializedData[serializedData.length] = {name:'_xfResponseType', value:'json'};

        serializedData[serializedData.length] = {name:'_xfToken', value:XF.config.csrf};

        serializedData[serializedData.length] = {name:'postid', value:button.attr('data-postid')};

        $.ajax({
            url:url,
            'success':function(response) {
                if(button.is(':visible'));
                button.parents('.actionBar-set--external').prepend(response.html.content);
                button.remove();
            },
            complete:function(response, status) {

            },
            data:serializedData,
            type:'GET',
            dataType:'json'
        });

    });
    $(document).on('click','.actionBar-action--briefing-remove', function(event) {
        event.preventDefault();
        button = $(this);
        var url = button.attr('data-href');

        var serializedData;

        serializedData = [];

        serializedData[serializedData.length] = {name:'_xfResponseType', value:'json'};

        serializedData[serializedData.length] = {name:'_xfToken', value:XF.config.csrf};

        serializedData[serializedData.length] = {name:'postid', value:button.attr('data-postid')};

        $.ajax({
            url:url,
            'success':function(response) {
                if(button.is(':visible'));
                button.parents('.actionBar-set--external').prepend(response.html.content);
                button.remove();


            },
            complete:function(response, status) {
            },
            data:serializedData,
            type:'GET',
            dataType:'json'
        });

    });
    $('.actionBar-action--briefing-add').each(function(){

        $(this).parents('.actionBar').find('.actionBar-set--external').prepend($(this));

    });
    $('.actionBar-action--briefing-remove').each(function(){

        $(this).parents('.actionBar').find('.actionBar-set--external').prepend($(this));

    });
    $('.attachment-icon').each(function(){

        var type = $(this).attr('data-extension');
        var el = $(this);
        if (type==='pdf'){
            var pdfUrl = $(this).find('a').attr('href');
            var container = $(this).parents('.message-userContent').attr('data-lb-id');

            //$(this).parents('section.message-attachments').append($('<canvas style="max-width:100%;" id='+id+'></canvas>'));

            // If absolute URL from the remote server is provided, configure the CORS
// header on that server.
            var url = XF.config.url.fullBase.replace(/\/$/, "")+pdfUrl;

// Loaded via <script> tag, create shortcut to access PDF.js exports.
            var pdfjsLib = window['pdfjs-dist/build/pdf'];

// The workerSrc property shall be specified.
            pdfjsLib.GlobalWorkerOptions.workerSrc = XF.config.url.fullBase +'js/XenCentral/XCBT/pdf.worker.js';

// Asynchronous download of PDF
            var loadingTask = pdfjsLib.getDocument(url);
            loadingTask.promise.then(function(pdf) {
                //console.log('PDF loaded');

                // Adjust how many pages should be shown
                var pageNumbers = [1,2,3];
                pageNumbers.forEach(function(number) {

                    pdf.getPage(number).then(function(page) {
                       // console.log('Page loaded');
                        var scale = 1.5;
                        var viewport = page.getViewport({scale: scale});
                        // Prepare canvas using PDF page dimensions
                        var id = 'canvas'+container + "_" +Math.floor(Math.random() * 5000);
                       // console.log(id);
                        el.parents('section.message-attachments').append($('<canvas style="max-width:100%;" id='+id+'></canvas>'))
                        var canvas = document.getElementById(id);
                        var context = canvas.getContext('2d');
                        canvas.height = viewport.height;
                        canvas.width = viewport.width;

                        // Render PDF page into canvas context
                        var renderContext = {
                            canvasContext: context,
                            viewport: viewport
                        };
                        var renderTask = page.render(renderContext);
                        renderTask.promise.then(function () {
                           // console.log('Page rendered');
                        });
                    }).catch(function(reason) {
                        //console.log('Handle rejected promise ('+reason+') here.');
                    });

                });


            }, function (reason) {
                // PDF loading error
                //console.error(reason);
            });
        }

    });
    $('.bbWrapper').find('a').each(function(){
        var href = $(this).attr('href');
        if(href) {
            if (href.includes('attachment')) {
                $(this).after('<img style="max-width:70%;" src="' + href + '" />');
                $(this).remove();
            }
        }

    });

})