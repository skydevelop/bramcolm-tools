<?php

namespace XenCentral\BramcolmTools\Pub\Controller;

use XF\Db\Exception;
use XF\Mvc\ParameterBag;
use XF\Pub\Controller\AbstractController;

class Room extends AbstractController
{
 public function actionIndex(ParameterBag $params){
   $vparams=[];
   $page = $this->filter('page','int');
   $perPage = $this->options()->messagesPerPage;
   $vparams['total'] = $this->finder('XenCentral\BramcolmTools:Briefing')->total();
   $vparams['page'] = $page;
		$vparams['perPage'] = $perPage;
	//change the true to false if you need to sort the posts by briefing room submitted time
		if(\XF::options()->xcbt_post_submit_time){
      $vparams['posts']= $this->finder('XenCentral\BramcolmTools:Briefing')->order('post_time', 'DESC')->limit($perPage, ($page * $perPage) - $perPage)->fetch();

    } else {
      $vparams['posts'] = $this->finder('XenCentral\BramcolmTools:Briefing')
        ->order('time', 'DESC')
        ->limit($perPage, ($page * $perPage) - $perPage)
        ->fetch();
    }
   foreach($vparams['posts'] as &$post){
     $vparams['posts_users'][$post['post_user_id']] = \XF::em()->find('XF:User', ['user_id'=>$post['post_user_id']]);
     $vparams['xf_posts'][$post['post_id']]=\XF::em()->find('XF:Post', ['user_id'=>$post['post_id']]);
   }
   return $this->view('XenCentral\BramcolmTools:Room','xcbt_room', $vparams);
 }

}