<?php

namespace XenCentral\BramcolmTools\Pub\Controller;

use XF\Db\Exception;
use XF\Mvc\ParameterBag;
use XF\Pub\Controller\AbstractController;

class Briefing extends AbstractController
{
 public function actionIndex(){}
  public function actionAdd(ParameterBag $params){
   if(\XF::visitor()->is_admin || \XF::visitor()->is_moderator) {
     $postid = $this->filter('postid', 'int');
     $briefing = $this->finder('XenCentral\BramcolmTools:Briefing')
       ->where('post_id', $postid)
       ->fetch();
     $post = $this->finder('XF:Post')->where('post_id', $postid)->fetch();


     if ($briefing->first() == FALSE && $post->first() != FALSE) {
       $post = $post->first();
       $thread = $this->finder('XF:Thread')->where('thread_id', $post['thread_id'])->fetch();
       $thread = $thread->first();
       $writer = \XF::em()->create('XenCentral\BramcolmTools:Briefing');
       $writer->set('post_user_id', $post['user_id']);
       $writer->set('post_id', $post['post_id']);
       $writer->set('author', $post['username']);
       $writer->set('submit_user_id', \XF::visitor()->user_id);
       $writer->set('title', '<a href='.\XF::app()->router('public')->buildLink('threads/post', $thread, ['post_id'=> $post['post_id']]).' >'.
       $thread['title'] . '</a>');
       $writer->set('message', substr($post['message'], 0, 300));
       $writer->set('time', \XF::$time);
       $writer->set('post_time', $post['post_date']);
       $writer->save();
     }
     $params=[];
     $params['postid']= $postid;
     return $this->view('XenCentral\BramcolmTools:Briefing','xcbt_successfully_added', $params);
   }
  }
  public function actionRemove(ParameterBag $params){
    if(\XF::visitor()->is_admin || \XF::visitor()->is_moderator) {
      $postid = $this->filter('postid', 'int');
      $briefing = $this->finder('XenCentral\BramcolmTools:Briefing')
        ->where('post_id', $postid)
        ->fetch();
      $post = $this->finder('XF:Post')->where('post_id', $postid)->fetch();

      if ($briefing->first() != FALSE && $post->first() != FALSE) {
        $briefing=$briefing->first();
        $writer = \XF::em()->find('XenCentral\BramcolmTools:Briefing', ['briefing_id'=>$briefing['briefing_id']]);
        $writer->delete();
      }
      $params=[];

      $params['postid']= $postid;
      return $this->view('XenCentral\BramcolmTools:Briefing','xcbt_successfully_removed',  $params);
    }

  }
}