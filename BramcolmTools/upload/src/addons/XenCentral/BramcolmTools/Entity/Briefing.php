<?php

namespace XenCentral\BramcolmTools\Entity;

use XF\Mvc\Entity\Entity;
use XF\Mvc\Entity\Structure;


class Briefing extends Entity
{
    public static function getStructure(Structure $structure)
    {

        $structure->table = 'xf_xcbt_briefing';
        $structure->shortName = 'XenCentral\BramcolmTools:Briefing';
        $structure->primaryKey = 'briefing_id';
        $structure->contentType = 'briefing_room';
        $structure->columns = [
            'briefing_id' => ['type' => self::UINT, 'autoIncrement' => true, 'maxLength' => 10, 'nullable' => false],
            'post_user_id' => ['type' => self::UINT, 'nullable' => false, 'maxLength' => 10
            ],
            'post_id' => ['type' => self::UINT, 'nullable' => false, 'maxLength' => 10
            ],
            'submit_user_id' => ['type' => self::UINT, 'nullable' => false, 'maxLength' => 10
            ],
            'title' => ['type' => self::STR, 'default' => '', 'nullable' => true, 'maxLength' => 255
            ],
            'message' => ['type' => self::STR
            ],
            'author' => ['type' => self::STR, 'maxLength' => 255],

            'time' => ['type' => self::UINT, 'nullable'=>false],

            'post_time' => ['type' => self::UINT, 'nullable'=>false],
        ];
        return $structure;
    }

}