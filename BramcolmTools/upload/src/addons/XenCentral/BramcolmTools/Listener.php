<?php


namespace XenCentral\BramcolmTools;


use XF\Db\Exception;

class Listener
{
    public static function redirectToLoginPage(\XF\Entity\User &$visitor)
    {
      $request= \XF::app()->request()->getRequestUri();
      $url = \XF::app()->request()->getFullRequestUri();
      $input = \XF::app()->request()->filter([
        'login' => 'str',
        'password' => 'str'
      ]);
      if(!$visitor->user_id){

              if($request !=='/index.php?login/login' && (empty($input['login']) || empty($input['password']))) {
                if (!strpos($url, 'admin.php')) {
                 header('Location: ' . \XF::options()->boardUrl . '/index.php?login/login');
                  exit;
                }
              }

        }
    }
    public static function addToPostBriefingInfo(\XF\Template\Templater $templater, &$type, &$template, &$name, array &$arguments, array &$globalVars){



        $briefing = \XF::finder('XenCentral\BramcolmTools:Briefing')->where('post_id', $arguments['post']['post_id'])->fetch();

        $briefing = $briefing->first();
        if($briefing != false){
          $arguments['briefing']=1;

        }
        $submitter = \XF::visitor()->is_admin || \XF::visitor()->is_moderator;
        $arguments['submitter']=$submitter;

        //dump($arguments);

    }


}