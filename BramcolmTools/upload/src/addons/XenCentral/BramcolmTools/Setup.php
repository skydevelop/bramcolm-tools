<?php
/**
 * @package XenCentral Feedback System
 * @author DNF Technology
 * @copyright Drnoyan & Nalyan LDA, Portugal, EU
 * @license http://dnf.technology/terms/
 * @link http://customers.dnf.technology
 * @version 2.1.6
 * @revision 29
 */

namespace XenCentral\BramcolmTools;

use XF\AddOn\AbstractSetup;
use XF\AddOn\StepRunnerInstallTrait;
use XF\AddOn\StepRunnerUninstallTrait;
use XF\AddOn\StepRunnerUpgradeTrait;
use XF\Db\Schema\Create;
use XF\Db\Schema\Alter;

class Setup extends AbstractSetup
{
    use StepRunnerInstallTrait;
    use StepRunnerUpgradeTrait;
    use StepRunnerUninstallTrait;

    public function installStep1(array $stepParams = [])
    {
        $sm = $this->schemaManager();

        foreach ($this->getTables() AS $tableName => $closure)
        {
            $sm->createTable($tableName, $closure);
        }
    }



    public function uninstallStep1()
    {
        $sm = $this->schemaManager();

        foreach (array_keys($this->getTables()) AS $tableName)
        {
            $sm->dropTable($tableName);
        }

    }


    protected function getTables()
    {
        $tables = [];

        $tables['xf_xcbt_briefing'] = function (Create $table) {
            $table->addColumn('briefing_id', 'int')->autoIncrement()->nullable(false)->unsigned();
            $table->addColumn('post_user_id', 'int')->nullable(false)->unsigned();
            $table->addColumn('post_id', 'int')->nullable(false)->unsigned();
            $table->addColumn('submit_user_id', 'int')->nullable(false)->unsigned();
            $table->addColumn('title', 'varchar', 255)->nullable(false);
            $table->addColumn('author', 'varchar', 255)->nullable(false);
            $table->addColumn('time', 'int')->nullable(false)->unsigned();
          $table->addColumn('post_time', 'int')->nullable(false)->unsigned();
          $table->addColumn('message', 'mediumtext')->nullable(false);
            $table->addPrimaryKey(['briefing_id']);
            $table->addKey(['time'], 'time');

        };
        return $tables;
    }
}